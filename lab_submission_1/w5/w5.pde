// COMP3419 WEEK06 TASK 1
// @Author : Evelyn Leslie

import processing.video.*; 
Movie m; 

int framenumber = 0; 
PImage next;
int BLUE = 120;

void setup() { 
  
  size(720,576); 
  frameRate(120); 
  m = new Movie(this, sketchPath("monkey.avi")); 
  m.frameRate(120); 

  stroke(255,255,255); //white coloured line
  m.play(); 
} 
 
void draw() { 
  
  float time = m.time();
  float duration = m.duration();

  if( time >= duration) { 
    //exit the program once the movie's finished
    exit();
  }
  
  if (m.available()){ 
      //while there's a next frame, saves it
      m.read();
      m.save(sketchPath("") + "BG/"+nf(framenumber, 4) + ".tif"); 
      
      if (framenumber > 1) {
          //loads the prevoius frame
          next = loadImage(sketchPath("") + "BG/"+nf(framenumber - 1, 4) + ".tif");
          int v [] = new int [3];
          
          image(next, 0, 0);
          //9x9 grid
          for (int x = 13; x < m.width-13; x+=18)
            for (int y = 13; y < m.height-13; y+=18){
              for(int j = 0; j < 3; j++){
                v[j] = 0;
              }
              int mloc = x + y * m.width;
              //compares SSD with 8 nearest neighbours
              for(int i = 0; i < 8; i++){
                int tmp [] = conv(m,x,y,next,i);
                //if the current neighbour has a smaller SSD than the prev, replace the coord
                if(v[2] < tmp[2] || v[2] == 0){
                  for(int k = 0; k < 3; k++){
                    v[k] = tmp[k];
                  }
                
                }
              }
              
              int min = v[2]; //SSD
              int x2 = v[0]; //neighour's x coor 
              int y2 = v[1]; //neighour's y coor 
                                           
              //finding boundaries                             
              if(BLUE > blue(m.pixels[mloc])){  
                for(int i = 1; i < 5; i++){
                if(blue(m.pixels[mloc + i]) > BLUE ||  blue(m.pixels[mloc - i]) > BLUE){
                  arrowdraw(x,y,x2+4,y2+4);
                }
                }
                
               }                                  
            }
        next.updatePixels();       
      }
    framenumber++; 
    } 
} 

void movieEvent(Movie m) { 
} 

int [] conv(PImage img, int x, int y, PImage K, int i){
   
    int rsum = 0;
    int gsum = 0;
    int bsum = 0;
    int min = 0;
    int ix = x-4;
    int iy = y-4;
    int x2 = 0;
    int y2 = 0;
    //checks the top left neighbour
    if(i ==0){
      x2 = ix - 9;
      y2 = iy - 9;
    }
    //checks the bottom right neighbour
    else if(i == 1){
      x2 = ix + 9;
      y2 = iy + 9;
    }
    //checks the right top neighbour
    else if(i == 2){
      x2 = ix + 9;
      y2 = iy - 9;
    }
    //checks the left bottom neighbour
    else if(i == 3){
      x2 = ix - 9;
      y2 = iy + 9;
    }
    //checks the left neighbour
    else if(i == 4){
      x2 = ix - 9;
      y2 = iy;
    }
    //checks the top neighbour
    else if(i == 5){
      y2 = iy - 9;
      x2 = ix;
    }
    //checks the right neighbour
    else if(i == 6){
      x2 = ix + 9;
      y2 = iy;
    }
    //checks the bottom neighbour
    else{
      y2 = iy + 9;
      x2 = ix;
    }
    int [] v = new int [3];
    
    v [1] = y2;
    v [0] = x2;
    
 
    for (int kx=0; kx<9; kx++, ix++, x2++)    
        for (int ky=0; ky<9; ky++, iy++, y2++) { 
             int loc = iy * img.width + ix;
             int mloc = x2 * K.width + y2;
             if(loc >= 414720 || mloc >= 414720){
               min = int(sqrt(rsum + gsum + bsum));
               v[2] = -1;
               break;
             }
             rsum += sq(int(red(img.pixels[loc]) - red(K.pixels[mloc])));
             gsum += sq(int(green(img.pixels[loc]) - green(K.pixels[mloc])));
             bsum += sq(int(blue(img.pixels[loc]) - blue(K.pixels[mloc])));
        }
        
     min = int(sqrt(rsum + gsum + bsum)); //SSD
     v[2] = min;
            
return v;
}

 void arrowdraw(int x1, int y1, int x2, int y2) { 
 int m = int(sqrt(sq(x1-x2)+sq(y1-y2)));  //magnitude
 pushMatrix();
 translate(x2, y2); 
 float a = atan2(x1-x2, y2-y1); 
 rotate(a); 
 line(0,0,m,0); //line of magnitude's length
 line(0, 0, -10, -10); 
 line(0, 0, 10, -10); 
 popMatrix(); 
 }